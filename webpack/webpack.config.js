/*-----------------------------------------------------------------------------
 * @package;    Kwaeri Web Developer Tools
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.4
 *---------------------------------------------------------------------------*/


module.exports =
{
    output:
    {
      filename: 'kwdt.min.js',
      sourceMapFilename: 'kwdt.min.js.map'
    },
    module:
    {
      rules:
      [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query:
          {
            presets:
            [
              ['es2015'],
            ],
          },
        },
      ],
    }
};