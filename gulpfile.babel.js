/*-----------------------------------------------------------------------------
 * @package:    Kwaeri Web Developer Tools - Build Automaton
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.4
 *---------------------------------------------------------------------------*/


 // Absolutely declare this always.
 'use strict';


// INCLUDES
import gulp from 'gulp';                            // Obviously :)
import gts from 'gulp-typescript';                  // For compiling typescript, requires the typescript module also.
import watch from 'gulp-watch';                     // For Auto-Compilation/Transpilation upon save
import sourcemaps from 'gulp-sourcemaps';           // For generating JavaScript source maps
import webpack from 'webpack';                      // For bundling... ; ;
import webpackStream from 'webpack-stream';         // So gulp can manipulate the webpack bundling process
import uglifyes from 'uglify-es';                   // To minify es6+ JavaScript syntax
import composer from 'gulp-uglify/composer';        // Allows gulp-uglify to use an alternate uglify module (default is uglify-js)
import plumber from 'gulp-plumber';                 // For helping gulp manipulate webpack
import rename from 'gulp-rename';                   // For adding suffixes/renaming files (we use it to name minified js files)
import del from 'del';                              // Used by our clean process, for the most part -
import bump from 'gulp-bump-version';               // Used for version increment automation
import runsequence from 'run-sequence';             // For running tasks sequentiallys


// GLOBALS
let minify = composer( uglifyes, console );         // Replacing the uglify processor with one which supports es6+
let gtsProject = gts.createProject( 'tsconfig.json' );  // A preliminary for transpiling typescript using microsoft's compiler

const args =                                        // Allows us to accept arguments to our gulpfile
(
    argList =>
    {
        let args = {}, i, option, thisOption, currentOption;

        for( i = 0; i < argList.length; i++ )
        {
            thisOption = argList[i].trim();

            option = thisOption.replace( /^\-+/, '' );

            if( option === thisOption )
            {
                // argument value
                if( currentOption )
                {
                    args[currentOption] = option;
                }

                currentOption = null;
            }
            else
            {
                currentOption = option;
                args[currentOption] = true;
            }
        }

        return args;
    }
)( process.argv );


// Check that we suppli
let bumpVersion         = args['bump-version'],
    toVersion           = args['version'],
    byType              = args['type'],
    bumpOptions         = ( bumpVersion ) ? { } : { type: 'patch' },
    projectBumpOptions  = ( bumpVersion ) ? { } : { type: 'patch', key: '"version": "' };

// If version was provided, overwrite the options accordingly
if( toVersion )
{
    bumpOptions = { version: toVersion };
    projectBumpOptions = bumpOptions;
    projectBumpOptions.key = '"version": "';
}

// If type was provided, overwrite the options accordingly
if( byType )
{
    bumpOptions = { type: byType };
    projectBumpOptions = bumpOptions;
    projectBumpOptions.key = '"version": "';
}


// Bump the version in our projects files (called manually, or through the build-release task)
gulp.task
(
    'bump-file-versions',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Modifying version strings in project files...', ' ' );

        // Otherwise, by providing --bump-version only, you will increment the patch revision

        // Start with all the files using the typical key, then hit out package.json: 'dist/src/**/*.js',
        return gulp.src
        (
            [
                '**/*.ts',
                '**/*.js',
                '**/*.scss',
                '!node_modules{,/**}'
            ],
            { base: './' }
        )
        .pipe( bump( bumpOptions ) )
        .pipe( gulp.dest( '' ) );
    }
);


// Bump the version in our in our package.json file
gulp.task
(
    'bump-project-version',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Modifying version string in package.json...', ' ' );

        // Target out package.json:
        return gulp.src
        (
            'package.json',
            { base: './' }
        )
        .pipe( bump( projectBumpOptions ) )
        .pipe( gulp.dest( '' ) );
    }
);


// Compile Typescript files
gulp.task
(
    'compile-typescript',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Compiling Typescript sources into JavaScript...', ' ' );

        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .js.pipe( gulp.dest( 'dist/js' ) );
    }
);


// Compile Typescript files
gulp.task
(
    'generate-typescript-declarations',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Generating Typescript declarations...', ' ' );

        return gtsProject.src() // This line pulls the 'src' from the tsconfig.json file
        .pipe( gtsProject() )
        .dts.pipe( gulp.dest( 'dist/js' ) );
    }
);


// Watches for changes in TS & SCSS files
gulp.task
(
    'watch',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Watching for changes to Typescript source...', ' ' );

        gulp.watch( 'src/**/*.ts', ['compile-typescript'] );
    }
);


// Bundle JS files with Webpack for use in a browser
gulp.task
(
    'transpile-library',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Transpiling and Bundling Kwaeri JavaScript using Webpack...', ' ' );

        gulp.src
        (
            'src/library.js'
        )
        .pipe( plumber() )  // Prevent stop on error
        .pipe( webpackStream( ( ( args.dev ) ? require( './webpack/webpack.dev.config' ) : require( './webpack/webpack.prod.config' ) ) ), webpack )
        .pipe( gulp.dest( 'dist/js/kwaeri' ) );
    }
);


// Optimzes JS files
gulp.task
(
    'optimize-js',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' ) + 'Minifying and mangling  JavaScript source files...', ' ' );

        return gulp.src( [ 'dist/js/**/*.js', '!dist/js/kwaeri/*.js' ] )
        .pipe( sourcemaps.init() )
        .pipe( minify() )
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( sourcemaps.write( '.', { includeContent: false, sourceRoot: '' } ) )
        .pipe( gulp.dest( 'dist/js' ) );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'clean:dist',
    () =>
    {
        console.log( ' ', ( ( args.alt ) ? '[Build-alt]: ' : '[Build]: ' )  + 'Cleaning build result...', ' ' );

        let deleteList = ( !args.alt ) ? 'dist' : 'dist';
        return del.sync
        (
            deleteList,
            { force: true }
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'build-release',
    callback =>
    {
        runsequence
        (
            'clean:dist',
            'bump-project-version',
            'bump-file-versions',
            'compile-typescript',
            'generate-typescript-declarations',
            'transpile-library',
            'optimize-js',
            callback
        );
    }
);


// Cleans the dist directory (removes it).
gulp.task
(
    'default',
    callback =>
    {
        runsequence
        (
            'clean:dist',
            'compile-typescript',
            'generate-typescript-declarations',
            'transpile-library',
            'optimize-js',
            callback
        );
    }
);

