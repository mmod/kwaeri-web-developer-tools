/**
 * Class for implementing the Kwaeri Web Developer Tools (kwdt)
 *
 * @since 0.1.0
 */
export declare class kwdt {
    /**
     * A container for queue elements which are queued
     *
     * @var array
     *
     * @since 0.1.0
     */
    private queueElements;
    /**
     * A container for the source of  queue elements which are queued
     *
     * @var array
     *
     * @since 0.1.0
     */
    private queueElementsSource;
    /**
     * A flag for the next API to use with the queue system
     *
     * @var void
     *
     * @since 0.1.0
     */
    private api;
    /**
     * A flag for the delay on the next queued action used by the queue system
     *
     * @var array
     *
     * @since 0.1.0
     */
    private interval;
    /**
     * A list of flags used with the queue system
     *
     * @var array
     *
     * @since 0.1.0
     */
    private flags;
    /**
     * A list of types for easing the implementation of our .type() method
     *
     * @var object
     *
     * @since 0.1.0
     */
    private classmap;
    /**
     * Class constructor
     *
     * @since 0.1.0
     */
    constructor();
    /**
     * Checks the type of any entity
     *
     * @param query The query to be type checked
     *
     * @returns string String The lowercase short variant of the type the entity evaluated to.
     *
     * @since 0.1.0
     */
    type(...args: any[]): string;
    /**
     * Checks if an object is empty or not
     *
     * @param object The object which we check is empty
     *
     * @return boolean true if empty, false otherwise
     *
     * @since 0.1.0
     */
    empty(...args: any[]): boolean;
    /**
     * An extend method that mimics jQuery and it's deep copy trait
     *
     * @param object a
     * @param object b
     *
     * @return object a extended by b
     *
     * @since 0.1.0
     */
    extend(...args: any[]): object;
    /**
     * Method for iterating over an object or list and performing an action for each index
     *
     * @param iteratee object|array The object or array being iterated over
     * @param applicator function   The action to apply to each index of the iteratee, can be suppieda key/value arguments
     *
     * @return boolean Returns true if all went well, otherwise false
     *
     * @since 0.1.0
     */
    each(iteratee: any, applicator: any): boolean;
    /**
     * Method which tests if a class exists on a DOM/HTML element
     *
     * @param entity HTMLElement|HTMLInputElement|any   The entity being tested for a specific class value
     * @param className string                          The class value being tested for existence on the entity provided
     *
     * @return boolean Returns true if the entity contains the class, false if not
     *
     * @since 0.1.0
     */
    hasClass(entity: any, className: any): any;
    /**
     * Method which adds a class to a DOM/HTML element
     *
     * @param entity HTMLElement|HTMLInputElement|any   The entity having a value added to its class attribute
     * @param className string                          The class value being added to the entity provided
     *
     * @return void
     *
     * @since 0.1.0
     */
    addClass(entity: any, className: any): void;
    /**
     * Method which removes a class from a DOM/HTML element
     *
     * @param entity HTMLElement|HTMLInputElement|any   The entity having a value removed from its class attribute
     * @param className string                          The class value being removed from the entity provided
     *
     * @return void
     *
     * @since 0.1.0
     */
    removeClass(entity: any, className: any): void;
    /**
     * A specialized method for checking if an entity/variable is a number
     *
     * @param entity
     *
     * @return boolean Returns true if the entity is a number, otherwise false
     *
     * @since 0.1.0
     */
    isNumber(entity: any): boolean;
    /**
     * Method that checks if an element is visible within the DOM
     *
     * @param entity HTMLElement - The entity for which visiblity is being checked fo
     *
     * @return boolean True if the entity is visible, otherwise false
     *
     * @since 0.1.0
     */
    isVisible(entity: HTMLElement): boolean;
    /**
     * Method to serialize data-attributes into an object
     *
     *      [NOTE]
     *      Strongly based upon the stackoverflow answer found here:
     *      https://stackoverflow.com/a/5915585
     *
     * @param entity HTMLElement The HTMLElement we are serializing the data-attributes of
     *
     * @return object An object which are the data-attributes of the provides element
     *
     * @since 0.1.0
     */
    data(entity: HTMLElement): object;
    /**
     * Method which contains the logic for processing items in the queue system
     *
     * @param void
     *
     * @return void
     *
     * @since 0.1.0
     */
    processing(): void;
    /**
     * Method which fills the queue system with newly queued items and manages flags for processing
     *
     * @param void
     *
     * @return void
     *
     * @since 0.1.0
     */
    filling(): void;
    /**
     * Method that activates and manages the queue system.
     *
     * [NOTES] To use this queue system, follow this usage template:
     *          {
     *              queue()('delay in seconds')('action after delay')();
     *          }
     *
     *   - You must always initially call: queue(),
     *   - After calling queue, you can THEN stack delay FIRST and method SECO-
     *     ND, alternating in that order ALWAYS.
     *   - At the end, you may optionally specify a callback in the form:
     *          {
     *              ( 'callback', function(){ // your code here } )
     *          }
     *   - You must ALWAYS trigger the queue with an empty function invocation:
     *     ();
     *   - Here is an example of a complete and full usage:
     *      {
     *        var qf = function(){ console.log( 'Queued method called!' ) },
     *            qf2 = function(){ console.log( 'Queued method 2 called!' ) },
     *            qcb = function(){ console.log( 'Queue finished! ); };
     *
     *          queue()('5000')(qf)('5000')(qf2)('callback', qcb)();
     *      }
     *   - Here is a most simple example:
     *      {
     *          var qf = function(){ console.log( 'Queued method called!' ) };
     *          queue()('5000')(qf)();
     *      }
     *   - You can loop the queue by passing ('loop') in place of a callback:
     *      {
     *          var qf = function(){ console.log( 'Queued method called!' ) };
     *          queue()('5000')(qf)('loop')();
     *      }
     *   - You can stop the queue by assigning the queue to a variable, then u-
     *     sing the variable like a function while passing stop:
     *      {
     *          var qf = function(){ console.log( 'Queued method called!' ) }
     *          var q = queue()('5000')(qf)();
     *              q('stop');
     *      }
     *
     * This queue system is based VERY strongly on:
     *      http://krasimirtsonev.com/blog/article/JavaScript-challenge-queue-implementation /
     *      https://github.com/krasimir/queue
     *
     * I felt the author did a very fine job creating a queue system, and after
     * studying it, felt no reason to reinvent his process other than to prope-
     * rly integrate it with the kwaeri web developer tools :)
     *      ~Thank you Krasimir Tsonev!~
     *
     * @param void
     *
     * @return function Returns a copy of itself to allow for chaining
     *
     * @since 0.1.0
     */
    queue(...args: any[]): Function;
    /**
     * Method to fetch and display a requested view
     *
     * @var options object              Defines an associative array of parameters
     *      @param function xhr             Defines a function which returns an XHR object, or null
     *      @param string   url             Defines the url the request is to
     *      @param string   method          Defines the request type
     *      @param boolean  asynchronous    Flags whether the request is synchronous or asynchronous
     *      @param string   contentType     wwDefines the type of content (i.e. normal, facility, ext, lbox, ibox etc)
     *      @param object   urlParameters   Defines any parts of a querystring
     *      @param object   postData        Defines any post data
     *      @param function beforeSend      Defines a function to run before making the requet
     *      @param function success         Defines a function to run after a successful request
     *      @param function error           Defines a function to run after a non-successful request
     *
     * @return void
     *
     * @since 0.1.0
     */
    ajax(args: object): void;
    /**
     * Initial parse of the response object returned from a request
     *
     * @param status string      The staus flag for the request (i.e 200, 404, 500 );
     * @param response
     *
     * @return void
     *
     * @since 0.0.1
     */
    receiveJSONResponse(status: any, response: any): object | boolean;
    /**
     * Method which supplements and aids in loading a script via the script() method
     *
     * @param args object An object specifying several details for loading a script
     *
     * @return void
     *
     * @since 0.1.0
     */
    loadScript(args: any): void;
    /**
     * Method to load an external script.
     *
     * @var object options      Defines an associative list of parameters.
     *      @param string file      Defines the file name of the external script to load ( extension included ).
     *      @param string type      File type, for navigating the nk-xrm assets structure.
     *      @param string system    System, manipulating the script source location.
     *      @param string ext       Ext, for navigation the nk-xrm assets structure.
     *
     * @return void
     *
     * @since 0.1.0
     */
    script(args: any): void;
    /**
     * Checks a chain of tests to see if they all passed
     * <p>
     * If any single test within the chain fails, we fail the whole chain. Also, if we have
     * more than one chain, we can pass true to the multi flag and process each chain
     *
     * @param chain object An object whose properties are singular tests from a particular unit
     * @param multi boolean True if there are multiple chains being passed in a single object
     *
     * @return boolean True if all tests passed, False if any test failed
     *
     * @since 0.1.0
     */
    testchain(chain: object, multi?: boolean): boolean;
}
