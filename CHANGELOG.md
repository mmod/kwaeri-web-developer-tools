# Changelog

## Unreleased

### Added

### Changed

### Removed

## 0.1.3 - 2018-06-04 20:39:52 EST

### Added

### Changed

* Updated `README.md`
  * Fixed the top donation image link as it was broken and not displaying the image correctly
* Updated `CHANGELOG.md` as the date and time of release was missing from version 0.1.2

### Removed

## 0.1.2 - 2018-06-04 20:35:36 EST

### Added

* Added gulp-bump-version dependency for automated file-header version increment in` gulpfile.babel.js`
* Added two new methods to `gulpfile.babel.js` which automate incrementing file and project versions, respectively
  * `bump-file-versions` automatically increments file versions located in the header comments of each source file
  * `bump-project-version` automatically increments the project version property value in `package.json`
* Added `CHANGELOG.md` for managing the changelog
  * Initial release added to the changelog
  * 0.1.1 release added to the changelog

### Changed

* Updated the `README.md` file
  * Bits which are static, but which are fully covered in the [documentation project wiki](https://gitlab.com/mmod/documentation/wikis/home) have been updated to reference the respective wiki pages
  * The link to the donation image used in the `README.md` file has been updated

### Removed

## 0.1.1 - 2018-05-28 11:53:00 EST

### Added

* Added new method `data()`
  * Reads all data-attribute methods from an HTMLElement and creates a dictionary from there
* Added typescript declaration files for `index.ts` and `src/kwdt.ts`
* Added `tester.html` for testing `kwdt.js` by itself via a browser console
* Added `index.ts` to serve `kwdt.js` from the dist directory as a module to other modules
  * Is compiled into `index.js`

### Changed

* Updated `test.ts`
  * Test added for the new data method
  * Test flow updated, as well as output of test results
  * Tests that fail now properly dislpay as failed and halt the test with an error
* Updated `library.js`
  * Import statements changed to be relative and call the main module file by name without extension
* Updated docs, specifically the `index.html` file
  * Added information about kwdt
  * Removed information about kux
  * Implemented the document flow that was added to the kwaeri-user-experience library based upon the document flow used in the material design website
  * Added sections and fragments based upon the required bits that will be used by this project
* Updated `tsconfig.json`
  * Added `declaration: true` to `comiplerOptions`
* Updated `README.md`, adding several bits that were not included previously, and updating several existing bits
  * Updated links to download the source code to the new project URL
  * Updaed headings, heading levels now step appropriately
  * Added Coding Standards
  * Added Bug Reports
  * Added Vulnerability Reports
  * Added Table of Contents
  * Added information regarding testing
  * Added link to a coverage badge for this project specifically
* Updated `license.txt`
  * References the proper project now under its new name

### Removed

* Removed `kux-bundle.md.css` from the docs directory
  * Now we link the kux bundle from its hosted project url via the raw version URL

### Initial Release

## 0.1.0 - 2018-05-29 11:53:00 EST

### Initial Release
