/*-----------------------------------------------------------------------------
 * @package;    Kwaeri Web Developer Tools
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.4
 *---------------------------------------------------------------------------*/


// INCLUDES


// GLOBALS


/**
 * Class for implementing the Kwaeri Web Developer Tools (kwdt)
 *
 * @since 0.1.0
 */
export class kwdt
{
    /**
     * A container for queue elements which are queued
     *
     * @var array
     *
     * @since 0.1.0
     */
    private queueElements = [];


    /**
     * A container for the source of  queue elements which are queued
     *
     * @var array
     *
     * @since 0.1.0
     */
    private queueElementsSource = [];


    /**
     * A flag for the next API to use with the queue system
     *
     * @var void
     *
     * @since 0.1.0
     */
    private api = null;


    /**
     * A flag for the delay on the next queued action used by the queue system
     *
     * @var array
     *
     * @since 0.1.0
     */
    private interval = null;


    /**
     * A list of flags used with the queue system
     *
     * @var array
     *
     * @since 0.1.0
     */
    private flags = {};


    /**
     * A list of types for easing the implementation of our .type() method
     *
     * @var object
     *
     * @since 0.1.0
     */
    private classmap =
    {
        "[object Boolean]": "boolean",
        "[object Number]": "number",
        "[object String]": "string",
        "[object Function]": "function",
        "[object Array]": "array",
        "[object Date]": "date",
        "[object RegExp]": "regexp",
        "[object Object]": "object",
        "[object NodeList]": "nodelist",
        "[object HTMLCollection]": 'htmlcollection'
    };


    /**
     * Class constructor
     *
     * @since 0.1.0
     */
    constructor()
    {
    }


    /**
     * Checks the type of any entity
     *
     * @param query The query to be type checked
     *
     * @returns string String The lowercase short variant of the type the entity evaluated to.
     *
     * @since 0.1.0
     */
    type( ...args: any[] ): string // Example of ES6 Spread Proposal with Type Annotations
    {
        // The working code below yields faster performance than the following snippet by almost 1.5 - 2 times the amount
        // ( http://stackoverflow.com/a/12022491 ):
        //
        // return ( o === null ) ? String( o ) : _type[ toString.call( o ) ] || "object";
        if( arguments[0] === null )
        {
            return String( arguments[0] );
        }
        else
        {
            return this.classmap[toString.call( arguments[0] )] || "unknown";
        }
    }


    /**
     * Checks if an object is empty or not
     *
     * @param object The object which we check is empty
     *
     * @return boolean true if empty, false otherwise
     *
     * @since 0.1.0
     */
    empty( ...args: any[] ): boolean
    {
        return !Object.keys( arguments[0] ).length;
    }


    /**
     * An extend method that mimics jQuery and it's deep copy trait
     *
     * @param object a
     * @param object b
     *
     * @return object a extended by b
     *
     * @since 0.1.0
     */
    extend( ...args: any[] ): object
    {
        for( let i = 1; i < arguments.length; i++ )
        {
            for( let key in arguments[i] )
            {
                if( arguments[i].hasOwnProperty( key ) )
                {
                    if( typeof arguments[0][key] === 'object' && typeof arguments[i][key] === 'object' )
                    {
                        this.extend( arguments[0][key], arguments[i][key] );
                    }
                    else
                    {
                        arguments[0][key] = arguments[i][key];
                    }
                }
            }
        }

        return arguments[0];
    }


    /**
     * Method for iterating over an object or list and performing an action for each index
     *
     * @param iteratee object|array The object or array being iterated over
     * @param applicator function   The action to apply to each index of the iteratee, can be suppieda key/value arguments
     *
     * @return boolean Returns true if all went well, otherwise false
     *
     * @since 0.1.0
     */
    each( iteratee, applicator ): boolean
    {
        // Ensure the second argument is a function
        if( this.type( applicator ) === "function" )
        {
            // Get the number of arguments expected
            let argumentCount = applicator.length;

            // Determine the type of the supplied object
            switch( this.type( iteratee ) )
            {
                case "htmlcollection":
                case "nodelist":
                case "array":
                {
                    // Iterate over the array
                    for( let i = 0; i < iteratee.length; i++ )
                    {
                        // Provide the index as the key to the provided function
                        if( argumentCount > 1 )
                        {
                            // Send the key and value
                            applicator( i, iteratee[i] );
                        }
                        else if( argumentCount === 1 )
                        {
                            // Send only the value
                            applicator( iteratee[i] );
                        }
                        else
                        {
                            applicator();
                        }
                    }
                }
                break;

                case "object":
                {
                    // Iterate over the objects properties
                    for( let property in iteratee )
                    {
                        // Supply the property name as the key to the provided function
                        if( argumentCount > 1 )
                        {
                            // Send the key and value
                            applicator( property, iteratee[property] );
                        }
                        else if( argumentCount === 1 )
                        { // Send only the value
                            applicator( iteratee[property] );
                        }
                        else
                        {
                            applicator();
                        }
                    }
                }
                break;
                default:
                {
                    // First argument invalid, must be an Object or an associative Array
                    console.log( 'Something went wrong with .each()', 'background: #000; color: #027;' )
                    //return false;
                }
            }
            // Everything went well
            return true;
        }
        // Second argument invalid, must be a function that takes 2 arguments
        return false;
    }


    /**
     * Method which tests if a class exists on a DOM/HTML element
     *
     * @param entity HTMLElement|HTMLInputElement|any   The entity being tested for a specific class value
     * @param className string                          The class value being tested for existence on the entity provided
     *
     * @return boolean Returns true if the entity contains the class, false if not
     *
     * @since 0.1.0
     */
    hasClass( entity, className )
    {
        return ( entity.classList ) ? entity.classList.contains( className ) : !!entity.className.match( new RegExp( '(\\s/^)' + className + '(\\s/$)'  ) );
    }


    /**
     * Method which adds a class to a DOM/HTML element
     *
     * @param entity HTMLElement|HTMLInputElement|any   The entity having a value added to its class attribute
     * @param className string                          The class value being added to the entity provided
     *
     * @return void
     *
     * @since 0.1.0
     */
    addClass( entity, className ): void
    {
        ( entity.classList ) ? entity.classList.add( className ) : entity.className += " " + className;
    }


    /**
     * Method which removes a class from a DOM/HTML element
     *
     * @param entity HTMLElement|HTMLInputElement|any   The entity having a value removed from its class attribute
     * @param className string                          The class value being removed from the entity provided
     *
     * @return void
     *
     * @since 0.1.0
     */
    removeClass( entity, className ): void
    {
        if( entity.classList )
        {
          entity.classList.remove( className );
        }
        else if( this.hasClass( entity, className ) )
        {
          var reg = new RegExp( '(\\s|^)' + className + '(\\s|$)' );
          entity.className = entity.className.replace( reg, ' ' );
        }
    }


    /**
     * A specialized method for checking if an entity/variable is a number
     *
     * @param entity
     *
     * @return boolean Returns true if the entity is a number, otherwise false
     *
     * @since 0.1.0
     */
    isNumber( entity ): boolean
    {
        return !isNaN( parseFloat( entity ) ) && isFinite( entity );
    }


    /**
     * Method that checks if an element is visible within the DOM
     *
     * @param entity HTMLElement - The entity for which visiblity is being checked fo
     *
     * @return boolean True if the entity is visible, otherwise false
     *
     * @since 0.1.0
     */
    isVisible( entity: HTMLElement ): boolean
    {
        return !!( entity.offsetWidth || entity.offsetHeight || entity.getClientRects().length );
    }


    /**
     * Method to serialize data-attributes into an object
     *
     *      [NOTE]
     *      Strongly based upon the stackoverflow answer found here:
     *      https://stackoverflow.com/a/5915585
     *
     * @param entity HTMLElement The HTMLElement we are serializing the data-attributes of
     *
     * @return object An object which are the data-attributes of the provides element
     *
     * @since 0.1.0
     */
    data( entity: HTMLElement ): object
    {
        // Define an object to hold all data attributes found on the provided element
        let datas = {};

        [].forEach.call
        (
            entity.attributes,
            function( dataAttribute )
            {
                if( /^data-/.test( dataAttribute.name ) )
                {
                    var camelCaseName = dataAttribute.name.substr( 5 ).replace
                    (
                        /-(.)/g,
                        function( $0, $1 )
                        {
                            return $1.toUpperCase();
                        }
                    );

                    datas[camelCaseName] = dataAttribute.value;
                }
            }
        );

        return datas;
    }


    /**
     * Method which contains the logic for processing items in the queue system
     *
     * @param void
     *
     * @return void
     *
     * @since 0.1.0
     */
    processing(): void
    {
        if( this.queueElements.length > 0 )
        {
            let item = this.queueElements.shift();

            if( ( this.flags as any ).stop !== true )
            {
                if( this.isNumber( item ) )
                {   // delay
                    this.interval = setTimeout( this.api, item );
                }
                else if( this.type( item ) === 'function' )
                {   // functions
                    item();
                    this.api();
                }
            }
            else
            {
                clearTimeout( this.interval );
            }
        }
        else
        {
            if( this.type( ( this.flags as any ).callback ) !== 'undefined' )
            {
                if( this.type( ( this.flags as any ).callback ) === 'function' )
                {
                    ( this.flags as any ).callback();
                }
            }

            if( ( this.flags as any ).loop )
            {
                this.queueElements = [];

                // We infinitely assign the value of this.queueElementsSource[i] to i, so that we 'loop' the process over and over.
                let element;
                for( let i=0; element = this.queueElementsSource[i]; i++ )
                {
                    this.queueElements.push( element );
                }

               this.api();
            }
        }
    }


    /**
     * Method which fills the queue system with newly queued items and manages flags for processing
     *
     * @param void
     *
     * @return void
     *
     * @since 0.1.0
     */
    filling(): void
    {
        let item = arguments[0];
        if( this.isNumber( item ) || this.type( item ) === 'function' )
        {
            this.queueElements.push( item );
            this.queueElementsSource.push( item );
        }
        else if( this.type( item ) === 'string' )
        {
            this.flags[item] = arguments[1] || true;
        }
    }


    /**
     * Method that activates and manages the queue system.
     *
     * [NOTES] To use this queue system, follow this usage template:
     *          {
     *              queue()('delay in seconds')('action after delay')();
     *          }
     *
     *   - You must always initially call: queue(),
     *   - After calling queue, you can THEN stack delay FIRST and method SECO-
     *     ND, alternating in that order ALWAYS.
     *   - At the end, you may optionally specify a callback in the form:
     *          {
     *              ( 'callback', function(){ // your code here } )
     *          }
     *   - You must ALWAYS trigger the queue with an empty function invocation:
     *     ();
     *   - Here is an example of a complete and full usage:
     *      {
     *        var qf = function(){ console.log( 'Queued method called!' ) },
     *            qf2 = function(){ console.log( 'Queued method 2 called!' ) },
     *            qcb = function(){ console.log( 'Queue finished! ); };
     *
     *          queue()('5000')(qf)('5000')(qf2)('callback', qcb)();
     *      }
     *   - Here is a most simple example:
     *      {
     *          var qf = function(){ console.log( 'Queued method called!' ) };
     *          queue()('5000')(qf)();
     *      }
     *   - You can loop the queue by passing ('loop') in place of a callback:
     *      {
     *          var qf = function(){ console.log( 'Queued method called!' ) };
     *          queue()('5000')(qf)('loop')();
     *      }
     *   - You can stop the queue by assigning the queue to a variable, then u-
     *     sing the variable like a function while passing stop:
     *      {
     *          var qf = function(){ console.log( 'Queued method called!' ) }
     *          var q = queue()('5000')(qf)();
     *              q('stop');
     *      }
     *
     * This queue system is based VERY strongly on:
     *      http://krasimirtsonev.com/blog/article/JavaScript-challenge-queue-implementation /
     *      https://github.com/krasimir/queue
     *
     * I felt the author did a very fine job creating a queue system, and after
     * studying it, felt no reason to reinvent his process other than to prope-
     * rly integrate it with the kwaeri web developer tools :)
     *      ~Thank you Krasimir Tsonev!~
     *
     * @param void
     *
     * @return function Returns a copy of itself to allow for chaining
     *
     * @since 0.1.0
     */
    queue( ...args: any[] ): Function
    {
        //let api  = null;
        let self = this;

        return self.api = function()
        {
            ( arguments.length === 0 ) ? self.processing() : self.filling.apply( self, arguments );

            return self.api;
        }

        //return this.api;
    }


    /**
     * Method to fetch and display a requested view
     *
     * @var options object              Defines an associative array of parameters
     *      @param function xhr             Defines a function which returns an XHR object, or null
     *      @param string   url             Defines the url the request is to
     *      @param string   method          Defines the request type
     *      @param boolean  asynchronous    Flags whether the request is synchronous or asynchronous
     *      @param string   contentType     wwDefines the type of content (i.e. normal, facility, ext, lbox, ibox etc)
     *      @param object   urlParameters   Defines any parts of a querystring
     *      @param object   postData        Defines any post data
     *      @param function beforeSend      Defines a function to run before making the requet
     *      @param function success         Defines a function to run after a successful request
     *      @param function error           Defines a function to run after a non-successful request
     *
     * @return void
     *
     * @since 0.1.0
     */
    ajax( args:object ): void
    {
        let defaults =
        {
            xhr:            null,
            url:            '/',
            method:         'GET',
            asynchronous:   true,
            contentType:    'application/x-www-form-urlencoded',
            urlParameters:  {},
            data:           {},
            beforeSend:     function(){ console.log( 'Submitting AJAX Request.' ); },
            success:        function(){ console.log( 'Status: ' + arguments[0] + ', Response: ' + arguments[1] ); },
            error:          function(){ console.error( 'Status: ' + arguments[0] + ', Response: ' + arguments[1] ); }
        };

        // Set up our options, extending the arguments provided by any required defaults that are missing.
        let options = this.extend( defaults, args );

        // Run the beforeSend method if it is not set to the default of 'false'
        if( ( options as any ).beforeSend )
        {
            ( options as any ).beforeSend();
        }

        // Prepare the URL
        let url = ( options as any ).url;

        // Build the query string if required
        if( !this.empty( ( options as any ).urlParameters ) )
        {
            let count = 0;
            this.each
            (
                ( options as any ).urlParameters,
                function( key, value )
                {
                    // This should be rather straight forward
                    url += ( ( count ) ? '&' : '?' ) + encodeURIComponent( key ) + '=' + encodeURIComponent( value );
                    count++;
                }
            );
        }

        // Declare and Initialize our XHR object
        let xhr = null;

        if( !( options as any ).xhr )
        {
            xhr = new XMLHttpRequest();
        }
        else
        {
            xhr = ( options as any ).xhr();
        }

        // Set the method and url and async flag in our XHR object
        xhr.open( ( options as any ).method, url, ( options as any ).asynchronous );

        // Set content type if required
        if( ( options as any ).method === 'POST' && !this.empty( ( options as any ).contentType ) )
        {
            xhr.setRequestHeader( 'Content-Type', ( options as any ).contentType );
        }

        // Build POST data if we are to POST
        let data = null;
        if( ( options as any ).method === 'POST' )
        {
            switch( ( options as any ).contentType )
            {
                case 'application/x-www-form-urlencoded':
                {
                    let count = 0;
                    this.each
                    (
                        ( options as any ).data,
                        function( key, value )
                        {
                            data = ( ( count ) ? '&' : '' ) + encodeURIComponent( key ) + '=' + encodeURIComponent( value );
                            count++;
                        }
                    );
                }break;

                case 'application/json':
                {
                    data = JSON.stringify( ( options as any ).data );
                }break;
            }
        }

        // Set a promise for when the request finishes
        xhr.onload = function()
        {
            //if( xhr.status === 200 )
            if( xhr.status < 400 )  // All direct error responses are > 400 i.e. 404, 500 etc... 200-300 are all response was returned
            {                       // Mostly it includes 200 (ok), 201 (created), etc...we can get really nitty gritty here...
                // Run the provided success method
                ( options as any ).success( xhr.status, xhr.responseText );
                //alert('User\'s name is ' + xhr.responseText);
            }
            else
            {
                // Run the provided error method
                ( options as any ).error( xhr.status, xhr.responseText );
                //alert('Request failed.  Returned status of ' + xhr.status);
            }
        };

        // Launch the request
        xhr.send( data );
    }


    /**
     * Initial parse of the response object returned from a request
     *
     * @param status string      The staus flag for the request (i.e 200, 404, 500 );
     * @param response
     *
     * @return void
     *
     * @since 0.0.1
     */
    receiveJSONResponse( status, response ): object|boolean
    {
        try
        {
            let responseObject = JSON.parse( response );

            return responseObject;
        }
        catch( e )
        {
            console.log( 'Error receiving response' );

            return false;
        }
    }


    /**
     * Method which supplements and aids in loading a script via the script() method
     *
     * @param args object An object specifying several details for loading a script
     *
     * @return void
     *
     * @since 0.1.0
     */
    loadScript( args ): void
    {
        //url is URL of external file, implementationCode is the code
        //to be called from the file, location is the location to
        //insert the <script> element
        let options = this.extend
        (
            {
                url: '',
                code: '',
                anchor: document.querySelector( 'body' )
            },
            args
        );

        let scriptTag = document.createElement( 'script' );
        scriptTag.src = ( options as any ).url;

        scriptTag.onload = ( options as any ).code;

        if( ( options as any ).code )
        {
            (scriptTag as any ).onreadystatechange = ( options as any ).code;
        }

        ( options as any ).anchor.appendChild( scriptTag );
    }


    /**
     * Method to load an external script.
     *
     * @var object options      Defines an associative list of parameters.
     *      @param string file      Defines the file name of the external script to load ( extension included ).
     *      @param string type      File type, for navigating the nk-xrm assets structure.
     *      @param string system    System, manipulating the script source location.
     *      @param string ext       Ext, for navigation the nk-xrm assets structure.
     *
     * @return void
     *
     * @since 0.1.0
     */
    script( args ): void
    {
        var options = this.extend
        (
            {
                // Default values
                file: "",
                type: null,
                system: "",
                extension: "",
                xrm: false
            },
            args
        );

        let currentURL = window.location.href + '/';
        let newURL = ( options as any ).file;

        if( ( options as any ).system === 'ext' )
        {
            var extensionPath = '';
            var extensionParts = ( options as any ).extension.split( '-' );
            if(  this.type( extensionParts ) === 'array' && extensionParts.length > -1 )
            {
                extensionPath = extensionParts[0] + '/' + extensionParts[0] + '_' + extensionParts[1];
                for( let i = 2; i < extensionParts.length; i++ )
                {
                    extensionPath += '_' + extensionParts[i];
                }
            }

            newURL = currentURL + "extensions/" + extensionPath + "/" + ( options as any ).type + "/" + ( options as any ).file;
        }

        this.loadScript( { url: newURL } );
    }


    /**
     * Checks a chain of tests to see if they all passed
     * <p>
     * If any single test within the chain fails, we fail the whole chain. Also, if we have
     * more than one chain, we can pass true to the multi flag and process each chain
     *
     * @param chain object An object whose properties are singular tests from a particular unit
     * @param multi boolean True if there are multiple chains being passed in a single object
     *
     * @return boolean True if all tests passed, False if any test failed
     *
     * @since 0.1.0
     */
    testchain( chain:object, multi:boolean=false ): boolean
    {
        // If any test in the chain failed, we fail the whole chain
        let temporaryResult = true;

        if( !multi )
        {
            // For a single chain we just run our logic
            for( let test in chain )
            {
                if( !chain[test].result )
                {
                    temporaryResult = false;
                }
            }
        }
        else
        {
            // If we have multiple chains
            for( var unit in chain )
            {
                // Call this method on each individual chain
                temporaryResult = ( this.testchain( chain[unit] ) );
            }
        }

        // The result will return as expected
        return temporaryResult;
    };
}