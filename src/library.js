/*-----------------------------------------------------------------------------
 * @package;    Kwaeri Web Developer Tools
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.4
 *---------------------------------------------------------------------------*/
'use strict'

// Import the Kwaeri Web Developer Tools
import { kwdt } from '../dist/js/src/kwdt';

// Instantiate the Kwaeri Web Developer Tools & Kwaeri User Experience Library
let _ = new kwdt();

// Ensure Kwaeri Web Developer Tools are available in the window instance
window._ = _;

// Good to go now!  kwdt.X is available now