# kwaeri-web-developer-tools (kwdt)

[![pipeline status](https://gitlab.com/mmod/kwaeri-web-developer-tools/badges/master/pipeline.svg)](https://gitlab.com/mmod/kwaeri-web-developer-tools/commits/master)  [![coverage report](https://gitlab.com/mmod/kwaeri-web-developer-tools/badges/master/coverage.svg)](https://mmod.gitlab.io/kwaeri-web-developer-tools/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

> WARNING! kwdt is not ready for production yet. We have decided to publish early for testing purposes during development. You are free to check us out, but please note that this project is by no means complete, nor safe, and we **DO NOT** recommend using kwdt at this time. With that said, please feel free to check the library out and see where it's headed!

The short version:

kwaeri-web-developer-tools (kwdt) is a minimal set of tools for aiding rapid development of application logic. It is a dependency of the kwaeri-user-experience library, the client-side counterpart of the nk application platform.

If you like our software, please consider making a donation. Donations help greatly to maintain the Massively Modified network and continued support of our open source offerings:

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

## Table of Contents

* [The Implementation (about kwdt)](#the-implementation)
  * [Developer Hype](#developers)
* [Using kwdt](#using-kwdt)
  * [Installation](#installation)
  * [Integration](#integration)
    * [JavaScript](#javascript)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

The library provides potentially familiar methods; `.type()`, `.isNumber()`, `isVisible()`, `.empty()`, `.extend()`, `.each()`, `data()`, `.hasClass()`, `.addClass()`, `.removeClass()`, `.ajax()`,  `.script()`  as well as a simple (though *powerful*) `.queue()` system.  We may very well add more tools to the collection as we continue to grow the project, though we've found that these tools more than suffice in helping us to rapidly develop logic on the client-side of any application.

Browse [documentation](https://mmod.gitlab.io/kwaeri-web-developer-tools/) for more information, including API reference (*still in development*).

### Developers

The project is heavily geared for developers and is completely customizable. Installation enables a seamless work-flow, wrapping SCSS, TypeScript, and webpack/babel bundle build processes with Gulp.

## Using kwdt

To use kwdt in your own project/application you'll need to get a copy of the distribution file(s), and include the library in any entry-point(s) to your application.

### Installation

You can obtain kwdt a couple of different ways:

* Via npm with `npm install @kwaeri/web-developer-tools` in a terminal/command prompt.
* Via direct download in several available formats:
  * [.zip](https://gitlab.com/mmod/kwaeri-web-developer-tools/-/archive/master/kwaeri-web-developer-tools-master.zip)
  * [.tar.gz](https://gitlab.com/mmod/kwaeri-web-developer-tools/-/archive/master/kwaeri-web-developer-tools-master.tar.gz)
  * [.tar.bz2](https://gitlab.com/mmod/kwaeri-web-developer-tools/-/archive/master/kwaeri-web-developer-tools-master.tar.bz2)
  * [.tar](https://gitlab.com/mmod/kwaeri-web-developer-tools/-/archive/master/kwaeri-web-developer-tools-master.tar)

With either option, after obtaining the source, the production files can be found within the `dist` subdirectory.

### Integration

Include the appropriate production files in your application's entry-point(s):

#### JavaScript

```html
<script src="../path/to/kwdt/dist/js/kwaeri/kwdt.min.js"></script>
```

Source maps are provided, so feel free to use the minified JavaScript for development.

The kwdt object is pre-initialized and ready to go, load your application in the Chrome browser and you can start testing in the debug console immediately!

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-web-developer-tools/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:incoming+mmod/kwaeri-web-developer-tools@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-web-developer-tools/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
