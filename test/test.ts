/*-----------------------------------------------------------------------------
 * @package;    gulp-bump-version
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.4
 *---------------------------------------------------------------------------*/


// INCLUDES
import * as assert from 'assert';
import { kwdt } from '../../../index.js';
import jsd from 'jsdom-global';


// GLOBALS
jsd();
let _ = new kwdt();


// SANITY CHECK - Makes sure our tests are working proerly
describe
(
    'SanityCheck', function()
    {
        describe
        (
            '#indexOf()',
            function()
            {
                it
                (
                    'should return -1 when the value is not present',
                    function( done )
                    {
                        assert.equal( [1,2,3].indexOf( 4 ), -1 );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check I
describe
(
    'kwdt.type() I',
    function()
    {
        describe
        (
            'Checking boolean type',
            function()
            {
                it
                (
                    'Should return true when checking the type of a true boolean object.',
                    function( done )
                    {
                        // Create the type:
                        let testType = true;

                        assert.equal
                        (
                            _.type( testType ),
                            'boolean'
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check II
describe
(
    'kwdt.type() II',
    function()
    {
        describe
        (
            'Checking number type',
            function()
            {
                it
                (
                    'Should return true when checking the type of a true number object.',
                    function( done )
                    {
                        // Create the type:
                        let testType = 1;

                        assert.equal
                        (
                            _.type( testType ),
                            'number'
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check III
describe
(
    'kwdt.type() III',
    function()
    {
        describe
        (
            'Checking string type',
            function()
            {
                it
                (
                    'Should return true when checking the type of a true string object.',
                    function( done )
                    {
                        // Create the type:
                        let testType = 'abcde';

                        assert.equal
                        (
                            _.type( testType ),
                            'string'
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check IV
describe
(
    'kwdt.type() IV',
    function()
    {
        describe
        (
            'Checking function type',
            function()
            {
                it
                (
                    'Should return true when checking the type of a true function object.',
                    function( done )
                    {
                        // Create the type:
                        let testType = function(){};

                        assert.equal
                        (
                            _.type( testType ),
                            'function'
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check V
describe
(
    'kwdt.type() V',
    function()
    {
        describe
        (
            'Checking array type',
            function()
            {
                it
                (
                    'Should return true when checking the type of a true array object.',
                    function( done )
                    {
                        // Create the type:
                        let testType = [];

                        assert.equal
                        (
                            _.type( testType ),
                            'array'
                        );

                        done();
                    }
                );
            }
        );
    }
);



// Feature Check VI
describe
(
    'kwdt.type() VI',
    function()
    {
        describe
        (
            'Checking date type',
            function()
            {
                it
                (
                    'Should return true when checking the type of a true date object.',
                    function( done )
                    {
                        // Create the type:
                        let testType = new Date();

                        assert.equal
                        (
                            _.type( testType ),
                            'date'
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check VII
describe
(
    'kwdt.type() VII',
    function()
    {
        describe
        (
            'Checking regexp type',
            function()
            {
                it
                (
                    'Should return true when checking the type of a true regexp object.',
                    function( done )
                    {
                        // Create the type:
                        let testType = /regexp/mg;

                        assert.equal
                        (
                            _.type( testType ),
                            'regexp'
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check VIII
describe
(
    'kwdt.type() VIII',
    function()
    {
        describe
        (
            'Checking object type',
            function()
            {
                it
                (
                    'Should return true when checking the type of a true object.',
                    function( done )
                    {
                        // Create the type:
                        let testType = {};

                        assert.equal
                        (
                            _.type( testType ),
                            'object'
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check IX
describe
(
    'kwdt.type() IX',
    function()
    {
        describe
        (
            'Checking type',
            function()
            {
                it
                (
                    'Should return false when checking the wrong type of each object.',
                    function( done )
                    {
                        // Create the type:
                        let booleanType = function(){},
                            numberType  = 'abcde',
                            stringType  = 1,
                            functionType= [],
                            arrayType   = new Date(),
                            regexpType  = true,
                            objectType  = /regexp/mg,
                            dateType    = {};

                        let result =
                        (
                            ( _.type( booleanType )   === 'boolean' )   &&
                            ( _.type( numberType )    === 'number' )    &&
                            ( _.type( stringType )    === 'string' )    &&
                            ( _.type( functionType )  === 'function' )  &&
                            ( _.type( arrayType )     === 'array' )     &&
                            ( _.type( regexpType )    === 'regexp' )    &&
                            ( _.type( objectType )    === 'object' )    &&
                            ( _.type( dateType )      === 'date' )

                        ) ? true : false;

                        assert.equal
                        (
                            result,
                            false
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check X
describe
(
    'kwdt.empty() I',
    function()
    {
        describe
        (
            'Checking if object empty',
            function()
            {
                it
                (
                    'Should return true when checking if object is empty',
                    function( done )
                    {
                        // Create the type:
                        let testVar = {};

                        assert.equal
                        (
                            _.empty( testVar ),
                            true
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XI
describe
(
    'kwdt.empty() II',
    function()
    {
        describe
        (
            'Checking if object empty',
            function()
            {
                it
                (
                    'Should return false when checking if object is empty',
                    function( done )
                    {
                        // Create the type:
                        let testVar = { 'name': 'tester' };

                        assert.equal
                        (
                            _.empty( testVar ),
                            false
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XII
describe
(
    'kwdt.extend() I',
    function()
    {
        describe
        (
            'Checking array extend',
            function()
            {
                it
                (
                    'Should return true when checking if array extended as expected',
                    function( done )
                    {
                        let arrayA          = ['first', 'second', ['third-first', 'third-second', ['third-third-first', 'third-third-second', 'third-third-third']], 'fourth', "fifth"];
                        let arrayB          = ['first', 'second', ['third-first', 'third-second', ['third-third-first', 'third-third-second', 'third-third-third', 'third-third-fourth']], 'fourth'];

                        // Proper tests always include the expected result as a reference to compare with
                        let expectedArray   = ['first', 'second', ['third-first', 'third-second', ['third-third-first', 'third-third-second', 'third-third-third', 'third-third-fourth']], 'fourth', "fifth"];

                        // And deliver the actual results to compare with
                        let resultingArray  = _.extend( arrayA, arrayB );

                        let testResult  =  ( JSON.stringify( expectedArray ) === JSON.stringify( resultingArray ) );

                        assert.equal
                        (
                           testResult,
                            true
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XIII
describe
(
    'kwdt.extend() II',
    function()
    {
        describe
        (
            'Checking object extend',
            function()
            {
                it
                (
                    'Should return true when checking if object extended as expected',
                    function( done )
                    {
                        let objectA = {
                            "first": "hello",
                            "second": "world",
                            "third": { "first": "hello", "second": "world", "third": { "first": "hello", "second": "world", "third": "!" } },
                            "fourth": "hahaha",
                            "fifth": { "first": "hello", "second": "world" },
                            "sixth": "Please."
                        };
                        let objectB = {
                            "first": "hello",
                            "second": "world",
                            "third": { "first": "hello", "second": { "first": "hello", "second": "world", "third": "!" }, "third": { "first": "hello", "second": "world", "third": "!", "fourth": "Ok" } },
                            "fourth": "hahaha",
                            "fifth": "Of course!"
                        };

                        // Proper tests always include the expected result as a reference to compare with
                        let expectedObject = {
                            "first": "hello",
                            "second": "world",
                            "third": { "first": "hello", "second": { "first": "hello", "second": "world", "third": "!" }, "third": { "first": "hello", "second": "world", "third": "!", "fourth": "Ok" } },
                            "fourth": "hahaha",
                            "fifth": "Of course!",
                            "sixth": "Please."
                        };

                        // And deliver the actual results to compare with
                        let resultingObject = _.extend( objectA, objectB );

                        let testResult  =  ( JSON.stringify( expectedObject ) === JSON.stringify( resultingObject ) );


                        assert.equal
                        (
                            testResult,
                            true
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XIV
describe
(
    'kwdt.each() I',
    function()
    {
        describe
        (
            'Checking each',
            function()
            {
                it
                (
                    'Should return true when checking if each array index had it first character transformed uppercase',
                    function( done )
                    {
                        // A list of some type to use in our test
                        let fruits = ["apple", "orange", "banana", "plum", "grape" ];

                        // Now we are going to loop throught the list of fruits and make another list of fruit-salad types
                        let saladTypes = [];

                        _.each
                        (
                            fruits,
                            function( index, fruit )
                            {
                                saladTypes[index] = fruit.charAt(0).toUpperCase() + fruit.slice(1) + ' Salad';
                            }
                        );

                        // We need a control to compare against in our test:
                        let expectedSaladTypes = ['Apple Salad', 'Orange Salad', 'Banana Salad', 'Plum Salad', 'Grape Salad'];

                        let testResult = ( JSON.stringify( saladTypes ) === JSON.stringify( expectedSaladTypes ) )

                        assert.equal
                        (
                            testResult,
                            true
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XV
describe
(
    'kwdt.hasClass() I',
    function()
    {
        describe
        (
            'Checking hassClass',
            function()
            {
                it
                (
                    'Should return true when checking if the element had the specified class',
                    function( done )
                    {
                        // Create a pseudo dom element for testing our hasClass() method
                        let pseudoElement = document.createElement( 'p' );

                        // Add a class to our psudo element so we can test if the psuedo element has a known class
                        pseudoElement.classList.add( 'test-class' );

                        // Now test that our pseudo element has the class:
                        let testResult = _.hasClass( pseudoElement, 'test-class' );

                        assert.equal
                        (
                            testResult,
                            true
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XVI
describe
(
    'kwdt.hasClass() II',
    function()
    {
        describe
        (
            'Checking hassClass',
            function()
            {
                it
                (
                    'Should return false when checking if the element had the specified class',
                    function( done )
                    {
                        // Create a pseudo dom element for testing our hasClass() method
                        let pseudoElement = document.createElement( 'p' );

                        // Now test that our pseudo element has the class:
                        let testResult = _.hasClass( pseudoElement, 'test-class' );

                        assert.equal
                        (
                            testResult,
                            false
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XVII
describe
(
    'kwdt.addClass() I',
    function()
    {
        describe
        (
            'Checking addClass',
            function()
            {
                it
                (
                    'Should return rue when checking if the element had the specified class added',
                    function( done )
                    {
                        // Create a pseudo dom element for testing our hasClass() method
                        let pseudoElement = document.createElement( 'p' );

                        // Add a class to our psudo element so we can test if the psuedo element has a known class
                        pseudoElement.classList.add( 'test-class' );

                        // Now test that our pseudo element has the class:
                        let testResult = _.hasClass( pseudoElement, 'test-class' );

                        assert.equal
                        (
                            testResult,
                            true
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XVIII
describe
(
    'kwdt.removeClass() I',
    function()
    {
        describe
        (
            'Checking removeClass',
            function()
            {
                it
                (
                    'Should return true when checking if the element had the specified class removed',
                    function( done )
                    {
                        // Create a pseudo dom element for testing our hasClass() method
                        let pseudoElement = document.createElement( 'p' );

                        // Add a class to our psudo element so we can test if the psuedo element has a known class
                        pseudoElement.classList.add( 'test-class' );

                        // Now test that our pseudo element has the class:
                        let hasTheClass = _.hasClass( pseudoElement, 'test-class' );

                        if( !hasTheClass )
                        {
                            // Force a failing test since the class wasnt initially added
                            assert.equal
                            (
                                true,
                                false
                            )
                        }

                        // Time to remove the class
                        _.removeClass( pseudoElement, 'test-class' );

                        // And check again:
                        let testResult = _.hasClass( pseudoElement, 'test-class' );

                        // Should be false if it was removed
                        assert.equal
                        (
                            testResult,
                            false
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XIX
describe
(
    'kwdt.data() I',
    function()
    {
        describe
        (
            'Checking data',
            function()
            {
                it
                (
                    'Should return true when checking if the element had the specified data attributes',
                    function( done )
                    {
                        // Create a pseudo dom element for testing our hasClass() method
                        let pseudoElement = document.createElement( 'p' );

                        // Add some attributes to the element so we can subsequently define a dictionary with the method we are testing
                        pseudoElement.setAttribute( 'data-first', 'first' );
                        pseudoElement.setAttribute( 'data-second', 'second' );
                        pseudoElement.setAttribute( 'data-third', 'third' );

                        // Now test our data method and see if we get a dictionary from it:
                        let dictionary = _.data( pseudoElement ),
                            expectedDictionary = { first: 'first', second: 'second', third: 'third' };

                        let testResult = ( JSON.stringify( dictionary ) === JSON.stringify( expectedDictionary ) );

                        // Should be false if it was removed
                        assert.equal
                        (
                            testResult,
                            true
                        );

                        done();
                    }
                );
            }
        );
    }
);


// Feature Check XX
describe
(
    'kwdt.ajax() I',
    function()
    {
        describe
        (
            'Checking ajax',
            function()
            {
                it
                (
                    'Should return true when checking the status of the ajax request to what we are expecting to get',
                    function( done )
                    {
                        // Run the AJAX test:
                        _.ajax
                        (
                            {
                                method: 'POST',
                                url: 'https://reqres.in/api/users',
                                contentType: 'application/x-www-form-urlencoded',
                                data: { name: 'paul rudd', movies: [ 'I Love You Man', 'Role Models' ] },
                                beforeSend: function()
                                {
                                },
                                success: function()
                                {
                                    var reply = JSON.parse( arguments[1] );

                                    if( !reply.hasOwnProperty( 'id' ) || !reply.hasOwnProperty( 'createdAt' ) )
                                    {
                                        // Fail the test!
                                        assert.equal
                                        (
                                            false,
                                            true
                                        );
                                    }

                                    let testResult = ( arguments[0] === 201 );

                                    // Should be true
                                    assert.equal
                                    (
                                        testResult,
                                        true
                                    );

                                    done();
                                }
                            }
                        );
                    }
                );
            }
        );
    }
);


// Feature Check XXI
describe
(
    'kwdt.receiveJSONResponse() I',
    function()
    {
        describe
        (
            'Checking receiveJSONResponse',
            function()
            {
                it
                (
                    'Should return true when comapring the ajax response to what we are expecting to get',
                    function( done )
                    {
                        // Run the AJAX test:
                        _.ajax
                        (
                            {
                                method: 'POST',
                                url: 'https://reqres.in/api/users',
                                contentType: 'application/x-www-form-urlencoded',
                                data: { name: 'paul rudd', movies: [ 'I Love You Man', 'Role Models' ] },
                                beforeSend: function()
                                {
                                },
                                success: function()
                                {
                                    var reply = _.receiveJSONResponse( arguments[0], arguments[1] );

                                    // We're using a static API for testing - so we know what to expect
                                    if( !reply.hasOwnProperty( 'id' ) || !reply.hasOwnProperty( 'createdAt' ) )
                                    {
                                        // Fail the test!
                                        assert.equal
                                        (
                                            false,
                                            true
                                        );
                                    }

                                    let testResult = ( "I Love You Man,Role Models" === ( reply as any ).movies );

                                    // Should be true
                                    assert.equal
                                    (
                                        testResult,
                                        true
                                    );

                                    done();
                                }
                            }
                        );
                    }
                );
            }
        );
    }
);
