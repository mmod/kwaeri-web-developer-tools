/*-----------------------------------------------------------------------------
 * @package:    Kwaeri Web Developer Tools
 * @author:     Richard B Winters
 * @copyright:  2015-2018 Massively Modified, Inc.
 * @license:    Apache-2.0
 * @version:    0.1.4
 *---------------------------------------------------------------------------*/


// Export the kwaeri Web Developer Tools from the dist directory
export { kwdt } from './dist/js/src/kwdt';